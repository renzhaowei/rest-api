from flask import Blueprint
from views import calculate_sum, hello

rest_bp = Blueprint('rest', __name__, url_prefix='')

rest_bp.add_url_rule('/', view_func=hello, methods=['GET'])
rest_bp.add_url_rule('/index', view_func=hello, methods=['GET'])
rest_bp.add_url_rule('/sum', view_func=calculate_sum, methods=['GET'])