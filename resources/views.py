from webargs import fields
from webargs.flaskparser import use_args


def hello():
    print 'a'
    return "hello world"

sum_args = {
    'value1': fields.Float(required=True),
    'value2': fields.Float(required=True)
}


@use_args(sum_args)
def calculate_sum(args):
    value1 = args['value1']
    value2 = args['value2']
    res = str(value1 + value2)
    return res