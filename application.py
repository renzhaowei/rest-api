from flask import Flask
from resources import rest_bp

def create_app():
    app = Flask(__name__)
    app.config.from_pyfile('config/config_dev.py')
    rest_bp.subdomain = 'api'
    app.register_blueprint(rest_bp)
    return app